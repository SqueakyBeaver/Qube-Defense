#ifndef RANDOM_HPP
#define RANDOM_HPP

#include <chrono>
#include <random>

namespace rand_gen {
// Generate a random number
float genRand(float low, float high);
} // namespace ran_gen

#endif